test: clean
	clitest --pre-flight 'alias todo-txt="todo-txt -d $(shell pwd)/tests/setup/todo.cfg"' --prefix '    ' README.rst

clean:
	git worktree remove -f tests/setup/subtasks/world-domination || true
	git branch -D todotxt || true
	rm -fr tests/setup/subtasks/ tests/setup/todo.txt tests/setup/done.txt tests/setup/report.txt tests/setup/todo.txt.bak
