=======
subtask
=======

This is an addon for `todo-txt` to work with sublists. These are stored as
separate `*.todo.txt` files in a subdirectory next to the `todo.txt` and `done.txt`.
These can be referenced from tasks.


Why?
====

`todo-txt` works very well will its two file `todo.txt` and `done.txt`, but is
a bit clumsy when you have multiple todo-files. There are the subcommands
`todo-txt listfile` and `todo-txt move` that work with alternative todo files,
but that is it. Other subcommands simply can't work on an alternative todo file
without moving the task to the `todo.txt` or changing an environment variable. This
addon makes working with many `todo.txt` easy and frictionless from the
`todo-txt` cli.


Installation
============

Clone this repository and link from your .todo.actions.d to subtask. It builds
on the `note` as well as the `again` addons so make sure they are installed,
too.


Usage
=====

There are two ways of specifying which SUBLIST you want to work with. Either
you can specify the ITEM# in the main todo.txt-file that references it or you
can specify the basename of the SUBLIST file name.



Getting started
---------------

Before we start we need to initialize subtask with the init command.
This creates the folder for saving the subtask lists in.

.. code::

    $ todo-txt subtask init    #=> --exit 0

For this example we assume we want to pack for a vacation. So let's add that to
our main todo.txt file:

.. code::

    $ todo-txt add Pack for vacation
    1 Pack for vacation
    TODO: 1 added.

Knowing that we have forgotten to pack something on our sunglasses on our last
vacation we decide to plan what to pack first. We could do this directly in our
main todo-list, but then we won't be able to find it in our next vacation. So
we decide to make a list that we can reuse:

.. code::

    $ todo-txt subtask vacation addlist
    $ todo-txt subtask vacation add Sunglases
    1 Sunglases
    VACATION: 1 added.
    $ todo-txt subtask vacation add Undergarments and socks
    2 Undergarments and socks
    VACATION: 2 added.
    $ todo-txt subtask vacation ls
    1 Sunglases
    2 Undergarments and socks
    --
    VACATION: 2 of 2 tasks shown

Now lets add a reference from or original todo to the new list, so we don't forget it:

.. code::

    $ todo-txt append 1 subtask:vacation
    1 Pack for vacation subtask:vacation
    $ todo-txt subtask 1 ls


Built-in help
-------------


.. code::

    $ todo-txt help subtask
      Work with subtasks:
        subtask init [new-addon-name]
           If the optional new-add-name is given also create a new 
           subcommand for todo-txt under that name. Creates the folder for
           saving the subtask-lists. 
        subtask init-git [NAME]
           Create a new worktree in the current git-repository from the
           todotxt branch (creating it if needed) and link NAME.todo.txt
           to its todo.txt. If NAME is not provides use toplevel git directory
           instead.
        subtask ls
           List all subtask-lists.
        subtask ITEM# addlist
          Adds a subtask todo-list to the task on line ITEM#.
        subtask LISTNAME addlist
          Create an empty LISTNAME todo-list.
        subtask SUBLIST expand
          Copy tasks from subtask-list SUBLIST to the main
          todo-list. Mark original task (if SUBLIST is given via ITEM#)
          with +expanded. Subtask-tasks with a priority of Z are not
          copied. Use this work-around if you reuse subtask-lists and want
          to temporarily comment out a subtask.
        subtask ITEM# start
          Same as expand, but also mark original task as done.
        subtask SUBLIST path
          Shows the path of the subtask-list SUBLIST.
        subtask donepath
          Shows the path of the done-file used for all subtask lists.
        subtask SUBLIST COMMANDS
          Run todo-txt COMMANDS on subtask-list SUBLIST.
        subtask SUBLIST push N
          Copy task N from subtask-list SUBLIST to main todo.txt
        subtask SUBLIST give N
          Move task N from subtask-list SUBLIST to main todo.txt
        subtask SUBLIST activate N
          Copy task N from subtask-list to main todo.txt and mark it as 
          done in the subtask-list.  If the task contains a again:-key 
          mark it as done with 'again' instead of 'do'.
        subtask SUBLIST pull N
          Copy task N from main todo.txt to the given subtask-list
        subtask SUBLIST take N
          Move task N from main todo.txt to the given subtask-list
        subtask SUBLIST many ...
          Like 'many subtask SUBLIST ...', but let 'many' do the search on 
          the SUBLIST (instead of the main todo.txt)
        subtask usage
          Shows this usage message.
        subtask pull
          Synonymous for '_subtask.sync pull'.
        subtask push
          Synonymous for '_subtask.sync push'.
        subtask sync
          Synonymous for '_subtask.sync sync'.
    

focus
-----

You can mark one of your todolists as the one you are working on instead of needing to specify it each time.

But before we can use `focus` we need to tell it in which file it can store its
current focus it. You will want to add an export line for `$FOCUS_CONFIG` to
your TODO-config.

.. 

    $ todo-txt focus ls
    Error: Need permission to write and execute file $FOCUS_CONFIG=''. Aborting.
    $ todo-txt focus init        #=> --exit 1

.. code::

    $ export FOCUS_CONFIG=$XDG_RUNTIME_DIR/todotxt-focus

And tell `focus` to create that file for us:

.. code::

    $ todo-txt focus init        #=> --exit 0

Now we can set the vocus to our vacation todo-list:

.. code::

    $ todo-txt focus ls
    1 Pack for vacation subtask:vacation
    --
    TODO: 1 of 1 tasks shown
    $ todo-txt subtask vacation focus set
    $ todo-txt focus ls
    1 Sunglases
    2 Undergarments and socks
    --
    VACATION: 2 of 2 tasks shown

And when we are done we can reset the focus back to our main todo.txt:

.. code::

    $ todo-txt focus set
    $ todo-txt focus ls
    1 Pack for vacation subtask:vacation
    --
    TODO: 1 of 1 tasks shown


git integration
---------------

Most of my work is structured in projects. These projects often have git
repositories, so it is nice to store them there and have an easy way to access
the todo items of a current repository.

..

    Examples here should be an allusion to Linus Torvalds the creator of git. See
    https://en.wikipedia.org/wiki/History_of_Linux for ideas.

.. code::

    $ todo-txt subtask init-git world-domination     #=> --exit 0

This has created a branch named `todotxt` and created a worktree inside our
subtask folder of our todos with our given name `world-domination`:

.. code::

    $ git branch
    * main
    + todotxt

    $ todo-txt subtask ls
    done.txt
    vacation.todo.txt
    world-domination
    world-domination.todo.txt

Now we have a new todo-txt directory that we can use while we are in our git repository...

.. code::

    $ git todotxt add Study computer science at Univerity of Helsinki
    1 Study computer science at Univerity of Helsinki
    TODO: 1 added.

We can also use subtask within it:

.. code::

    $ git todotxt subtask init     #=> --exit 0
    $ git todotxt subtask freax addlist
    $ git todotxt subtask freax add Announce on comp.os.minix
    1 Announce on comp.os.minix
    FREAX: 1 added.

Its main todo.txt file (unlike the subtasks) we can access from anywhere with todo-txt:

.. code::

    $ cd /
    $ todo-txt subtask world-domination ls
    1 Study computer science at Univerity of Helsinki
    --
    WORLD-DOMINATION: 1 of 1 tasks shown

If you like keeping your issues inside your git repository, but want a more complete bug tracker including syncing with github's and gitlab's try git-bug_ instead.

.. _git-bug: https://github.com/MichaelMure/git-bug
